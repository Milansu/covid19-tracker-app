import 'package:covid19tracker/features/data/datasources/covid_remote_data_source.dart';
import 'package:covid19tracker/features/domain/repositories/covid_repository.dart';
import 'package:covid19tracker/features/domain/usecases/get_covid_summary.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'core/network/network_info.dart';
import 'features/data/datasources/covid_local_data_source.dart';
import 'features/data/repositories/covid_repository_impl.dart';
import 'features/presentation/bloc/covid_bloc.dart';
import 'util/app_theme.dart';
import 'package:covid19tracker/features/domain/usecases/set_favorite_country.dart';
import 'package:covid19tracker/features/domain/usecases/get_favorite_country.dart';
import 'package:covid19tracker/features/presentation/app_state.dart';
final sl = GetIt.instance;
final appTheme = sl<CovidTheme>();

Future<void> init() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  //! Features - Number Trivia
  // Bloc
  sl.registerFactory(
        () => CovidBloc(sl(), sl(), sl(), sl()),
  );

  // Use cases
  sl.registerLazySingleton(() => GetCovidSummary(sl()));
  sl.registerLazySingleton(() => GetFavoriteCountry(sl()));
  sl.registerLazySingleton(() => SetFavoriteCountry(sl()));

  sl.registerLazySingleton(() => AppState());

  // Repository
  sl.registerLazySingleton<CovidRepository>(
        () => CovidRepositoryImpl(sl(), sl(), sl()),
  );

  // Data sources
  sl.registerLazySingleton<CovidRemoteDataSource>(
        () => CovidRemoteDataSourceImpl(client: sl()),
  );

  sl.registerLazySingleton<CovidLocalDataSource>(
        () => CovidLocalDataSourceImpl(sharedPreferences: sharedPreferences),
  );
  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //! External
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
  sl.registerLazySingleton(() => CovidTheme());
}
