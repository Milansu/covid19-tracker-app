import 'package:covid19tracker/features/presentation/bloc/bloc.dart';
import 'package:covid19tracker/features/presentation/pages/covid_detail_page.dart';
import 'package:covid19tracker/features/presentation/pages/covid_intro_page.dart';
import 'package:covid19tracker/service_locator.dart';
import 'package:covid19tracker/util/shared.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:theme_provider/theme_provider.dart';

import 'features/presentation/pages/covid_favorite_page.dart';
import 'features/presentation/pages/covid_learn_more_page.dart';
import 'features/presentation/pages/covid_settings_page.dart';
import 'features/presentation/pages/covid_summary_page.dart';
import 'service_locator.dart' as di;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  CovidBloc covidBloc = sl<CovidBloc>();
  int _currentTabIndex = 1;

  @override
  void initState() {
    covidBloc.add(InitialCovidEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ThemeProvider(
      saveThemesOnChange: true,
      loadThemeOnInit: true,
      themes: <AppTheme>[
        AppTheme.light(),
        AppTheme.dark(),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: ThemeConsumer(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: PreferredSize(
              child: AppBar(
                title: Text('Covid19 Application'),
                actions: [
                  InkWell(
                    child: Icon(Icons.settings),
                    onTap: () => covidBloc.add(ShowSettingsPageEvent()),
                  )
                ],
                centerTitle: true,
              ),
              preferredSize: Size(0, 40),
            ),
            bottomNavigationBar: BottomNavigationBar(
              items: SharedData.bottomNavigationButtons,
              currentIndex: _currentTabIndex,
              type: BottomNavigationBarType.fixed,
              onTap: (int index) {
                setState(() {
                  _currentTabIndex = index;
                });
                covidBloc.add(NavigationButtonEvent(currentTab: index));
              },
            ),
            body: BlocProvider(
                create: (context) => covidBloc,
                child: BlocBuilder<CovidBloc, CovidState>(
                  bloc: covidBloc,
                  condition: (previouState, state) {
                    if (previouState is! FavoriteCountriesState &&
                        state is LikeCountryState) {
                      return false;
                    }
                    return true;
                  },
                  builder: (BuildContext context, CovidState covidState) {
                    if (covidState is InitialCovidState) {
                      return CovidIntroPage();
                    } else if (covidState is LoadedCovidState) {
                      return CovidSummaryPage(
                        covidSummary: covidState.covidSummary,
                      );
                    } else if (covidState is LoadedCovidByCountryState) {
                      return CovidDetailPage(covidState.country);
                    } else if (covidState is LoadedLearnMoreState) {
                      return LearnMorePage();
                    } else if (covidState is ErrorCovidState) {
                      return Center(
                        child: Text(covidState.message),
                      );
                    } else if (covidState is ShowSettingsPageState) {
                      return SettingsPage();
                    } else if (covidState is FavoriteCountriesState) {
                      return CovidFavoritePage(country: covidState.country);
                    }
                    return Container();
                  },
                )),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    covidBloc.close();
    super.dispose();
  }
}
