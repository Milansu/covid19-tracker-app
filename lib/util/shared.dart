import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SharedData {
  static final formatter = NumberFormat("#,###");

  static final bottomNavigationButtons = <BottomNavigationBarItem>[
    BottomNavigationBarItem(
        icon: Icon(Icons.favorite), title: Text("Favorites")),
    BottomNavigationBarItem(icon: Icon(Icons.home), title: Text("Home")),
  ];

  static final String doctorMan = 'assets/images/doctor-man.svg';
  static final String doctorWoman = 'assets/images/doctor-woman.svg';
  static final String maskMan = 'assets/images/mask-man.svg';
  static final String maskWoman = 'assets/images/mask-woman.svg';
  static final String remoteWorkMan = 'assets/images/remote-work-man.svg';
  static final String remoteWorkWoman = 'assets/images/remote-work-woman.svg';

  static final List<String> stayHome = [
    "Stay home. Most people with COVID-19 have mild illness and can recover at home without medical care. Do not leave your home, except to get medical care. Do not visit public areas.",
    "Take care of yourself. Get rest and stay hydrated. Take over-the-counter medicines, such as acetaminophen, to help you feel better."
        "Stay in touch with your doctor. Call before you get medical care. Be sure to get care if you have trouble breathing, or have any other emergency warning signs, or if you think it is an emergency.",
    "Avoid public transportation, ride-sharing, or taxis."
  ];

  static final List<String> separateYourself = [
    "As much as possible, stay in a specific room and away from other people and pets in your home. If possible, you should use a separate bathroom. If you need to be around other people or animals in or outside of the home, wear a cloth face covering.",
    "Additional guidance is available for those living in close quarters and shared housing.",
    "See COVID-19 and Animals if you have questions about pets."
  ];

  static final List<String> monitorSymptoms = [
    "Symptoms of COVID-19 fever, cough, or other symptoms.",
    "Follow care instructions from your healthcare provider and local health department. Your local health authorities may give instructions on checking your symptoms and reporting information.",
  ];

  static final List<String> callAhead = [
    "Call ahead. Many medical visits for routine care are being postponed or done by phone or telemedicine.",
    "If you have a medical appointment that cannot be postponed, call your doctor’s office, and tell them you have or may have COVID-19. This will help the office protect themselves and other patients."
  ];

  static final List<String> sickWear = [
    "You should wear a cloth face covering, over your nose and mouth if you must be around other people or animals, including pets (even at home)",
    "You don’t need to wear the cloth face covering if you are alone. If you can’t put on a cloth face covering (because of trouble breathing, for example), cover your coughs and sneezes in some other way. Try to stay at least 6 feet away from other people. This will help protect the people around you.",
    "Cloth face coverings should not be placed on young children under age 2 years, anyone who has trouble breathing, or anyone who is not able to remove the covering without help.",
  ];

  static final List<String> cover = [
    "Cover your mouth and nose with a tissue when you cough or sneeze.",
    "Throw away used tissues in a lined trash can.",
    "Immediately wash your hands with soap and water for at least 20 seconds. If soap and water are not available, clean your hands with an alcohol-based hand sanitizer that contains at least 60% alcohol."
  ];

  static final List<String> cleanHands = [
    "Wash your hands often with soap and water for at least 20 seconds. This is especially important after blowing your nose, coughing, or sneezing; going to the bathroom; and before eating or preparing food.",
    "Use hand sanitizer if soap and water are not available. Use an alcohol-based hand sanitizer with at least 60% alcohol, covering all surfaces of your hands and rubbing them together until they feel dry.",
    "Soap and water are the best option, especially if hands are visibly dirty.",
    "Avoid touching your eyes, nose, and mouth with unwashed hands.",
  ];

  static final List<String> avoidSharing = [
    "Do not share dishes, drinking glasses, cups, eating utensils, towels, or bedding with other people in your home.",
    "Wash these items thoroughly after using them with soap and water or put in the dishwasher."
  ];

  static final List<String> clean = [
    "Clean and disinfect high-touch surfaces in your “sick room” and bathroom; wear disposable gloves. Let someone else clean and disinfect surfaces in common areas, but you should clean your bedroom and bathroom, if possible.",
    "If a caregiver or other person needs to clean and disinfect a sick person’s bedroom or bathroom, they should do so on an as-needed basis. The caregiver/other person should wear a mask and disposable gloves prior to cleaning. They should wait as long as possible after the person who is sick has used the bathroom before coming in to clean and use the bathroom.",
    "Clean and disinfect areas that may have blood, stool, or body fluids on them.",
    "Use household cleaners and disinfectants. Clean the area or item with soap and water or another detergent if it is dirty. Then, use a household disinfectant.",
  ];
}
