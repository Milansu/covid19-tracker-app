import 'package:flutter/cupertino.dart';

abstract class AppTheme {
  TextStyle defaultTextStyle;
  TextStyle titleTextStyle;
}

class CovidTheme implements AppTheme {
  @override
  TextStyle defaultTextStyle = TextStyle(fontSize: 20.0);

  @override
  TextStyle titleTextStyle = TextStyle(fontSize: 22.0);
}
