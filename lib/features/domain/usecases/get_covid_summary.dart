import 'package:covid19tracker/core/error/failures.dart';
import 'package:covid19tracker/core/usecases/usecase.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:covid19tracker/features/domain/repositories/covid_repository.dart';
import 'package:dartz/dartz.dart';

class GetCovidSummary implements UseCase<CovidSummary> {
  final CovidRepository _covidRepository;

  GetCovidSummary(this._covidRepository);

  @override
  Future<Either<Failure, CovidSummary>> call() async {
    return await _covidRepository.getCovidSummary();
  }
}
