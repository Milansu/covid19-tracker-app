import 'package:covid19tracker/core/error/failures.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:covid19tracker/features/domain/repositories/covid_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

class SetFavoriteCountry {
  final CovidRepository _covidRepository;

  SetFavoriteCountry(this._covidRepository);

  Future<Either<Failure, bool>> call({@required Country country, bool removeItem}) async {
    return await _covidRepository.setFavoriteCountry(country: country, removeItem: removeItem);
  }
}
