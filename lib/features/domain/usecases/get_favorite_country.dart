import 'package:covid19tracker/core/error/failures.dart';
import 'package:covid19tracker/core/usecases/usecase.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:covid19tracker/features/domain/repositories/covid_repository.dart';
import 'package:dartz/dartz.dart';

class GetFavoriteCountry implements UseCase<List<Country>> {
  final CovidRepository _covidRepository;

  GetFavoriteCountry(this._covidRepository);

  @override
  Future<Either<Failure, List<Country>>> call() async {
    return await _covidRepository.getFavoriteCountry();
  }
}
