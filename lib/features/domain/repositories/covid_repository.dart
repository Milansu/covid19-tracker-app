import 'package:covid19tracker/core/error/failures.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

abstract class CovidRepository {
  Future<Either<Failure, CovidSummary>> getCovidSummary();
  Future<Either<Failure, List<Country>>> getFavoriteCountry();
  Future<Either<Failure, bool>> setFavoriteCountry({@required Country country, bool removeItem});
}