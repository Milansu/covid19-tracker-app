import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class CovidSummary extends Equatable {
  CovidSummary({
    @required this.global,
    @required this.countries,
    @required this.date,
  });

  final Global global;
  final List<Country> countries;
  final DateTime date;

  @override
  List<Object> get props => [global, countries, date];
}

class Country extends Equatable {
  Country({
    @required this.country,
    @required this.countryCode,
    @required this.slug,
    @required this.newConfirmed,
    @required this.totalConfirmed,
    @required this.newDeaths,
    @required this.totalDeaths,
    @required this.newRecovered,
    @required this.totalRecovered,
    @required this.date,
  });

  final String country;
  final String countryCode;
  final String slug;
  final int newConfirmed;
  final int totalConfirmed;
  final int newDeaths;
  final int totalDeaths;
  final int newRecovered;
  final int totalRecovered;
  final DateTime date;

  @override
  List<Object> get props => [
        country,
        countryCode,
        slug,
        newConfirmed,
        totalConfirmed,
        newDeaths,
        totalDeaths,
        newRecovered,
        totalRecovered,
      ];
}

class Global extends Equatable {
  Global({
    @required this.newConfirmed,
    @required this.totalConfirmed,
    @required this.newDeaths,
    @required this.totalDeaths,
    @required this.newRecovered,
    @required this.totalRecovered,
  });


  final int newConfirmed;
  final int totalConfirmed;
  final int newDeaths;
  final int totalDeaths;
  final int newRecovered;
  final int totalRecovered;

  @override
  List<Object> get props => [
        newConfirmed,
        totalConfirmed,
        newDeaths,
        totalDeaths,
        newRecovered,
        totalRecovered
      ];
}
