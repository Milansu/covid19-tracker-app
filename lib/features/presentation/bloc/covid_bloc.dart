import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:covid19tracker/core/error/failures.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:covid19tracker/features/domain/usecases/get_covid_summary.dart';
import 'package:covid19tracker/features/domain/usecases/set_favorite_country.dart';
import 'package:covid19tracker/features/domain/usecases/get_favorite_country.dart';
import 'package:covid19tracker/features/presentation/app_state.dart';
import 'package:dartz/dartz.dart';

import './bloc.dart';

class CovidBloc extends Bloc<CovidEvent, CovidState> {
  final GetCovidSummary _getCovidSummary;
  final SetFavoriteCountry _setFavoriteCountry;
  final GetFavoriteCountry _getFavoriteCountry;
  final AppState _appState;

  CovidBloc(this._getCovidSummary, this._setFavoriteCountry,
      this._getFavoriteCountry, this._appState);

  @override
  CovidState get initialState => InitialCovidState();

  @override
  Stream<CovidState> mapEventToState(
    CovidEvent event,
  ) async* {
    if (event is GetCovidSummaryEvent) {
      final _covidSummaryOrNull = await _getCovidSummary();
      if (_covidSummaryOrNull.isLeft()) {
        yield ErrorCovidState(message: "Error message");
        return;
      }
      final _covidOrNull = _covidSummaryOrNull.getOrElse(() => null);
      yield LoadedCovidState(_covidOrNull);
    } else if (event is GetCovidSummaryByCountryEvent) {
      yield LoadedCovidByCountryState(event.country);
      yield* _toggleFavoriteCountry(event);
    } else if (event is GetLearnMorePageEvent) {
      yield LoadedLearnMoreState();
    } else if (event is ShowSettingsPageEvent) {
      yield ShowSettingsPageState();
    } else if (event is NavigationButtonEvent) {
      if (event.currentTab == 1) {
        yield InitialCovidState();
        return;
      } else if (event.currentTab == 0) {
        final Either<Failure, List<Country>> _setFavorite =
            await _getFavoriteCountry();
        final List<Country> _countryOrNull = _setFavorite.getOrElse(() => null);
        _appState.setFavoriteCountries = _countryOrNull;
        if (_setFavorite.isLeft()) {
          yield ErrorCovidState(message: _setFavorite.hashCode.toString());
          return;
        }
        yield FavoriteCountriesState(_countryOrNull);
        return;
      }
    } else if (event is SetFavoriteCountryEvent) {
      if (_appState.getFavoriteCountries != null &&
          _appState.getFavoriteCountries.isNotEmpty &&
          _checkIfCountryIsFavorite(
              _appState.getFavoriteCountries, event.country)) {
        print("FALSE");
        yield LikeCountryState(false);
        await _setFavoriteCountry(country: event.country, removeItem: true);
      } else {
        print("true");
         await _setFavoriteCountry(
            country: event.country, removeItem: false);
        yield LikeCountryState(true);
      }
    }
  }

  Stream<CovidState> _toggleFavoriteCountry(
      GetCovidSummaryByCountryEvent event) async* {
    final Either<Failure, List<Country>> _setFavorite =
        await _getFavoriteCountry();
    final List<Country> _countryOrNull = _setFavorite.getOrElse(() => null);
    _appState.setFavoriteCountries = _countryOrNull;

    if (_checkIfCountryIsFavorite(_countryOrNull, event.country)) {
      yield LikeCountryState(true);
    } else {
      yield LikeCountryState(false);
    }
  }
}

bool _checkIfCountryIsFavorite(List<Country> countries, Country country) {
  for (int i = 0; i < countries.length; i++) {
    if (countries[i].country == country.country) {
      return true;
    }
  }
  return false;
}
