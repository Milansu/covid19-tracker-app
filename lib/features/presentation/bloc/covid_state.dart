import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

@immutable
abstract class CovidState extends Equatable {}

class InitialCovidState extends CovidState {
  @override
  List<Object> get props => [];
}

class LoadedCovidState extends CovidState {
  final CovidSummary covidSummary;

  LoadedCovidState(this.covidSummary);

  LoadedCovidState copyWith({CovidSummary covidSummary}) {
    return LoadedCovidState(covidSummary);
  }

  @override
  List<Object> get props => [covidSummary];
}

class LoadedCovidByCountryState extends CovidState {
  final Country country;

  LoadedCovidByCountryState(this.country);

  @override
  List<Object> get props => [country];
}

class LoadedLearnMoreState extends CovidState {
  @override
  List<Object> get props => [];
}

class ErrorCovidState extends CovidState {
  final String message;

  ErrorCovidState({@required this.message});

  @override
  List<Object> get props => [message];
}

class ShowSettingsPageState extends CovidState {
  @override
  List<Object> get props => [];
}

class FavoriteCountriesState extends CovidState {

  final List<Country> country;

  FavoriteCountriesState(this.country);

  @override
  List<Object> get props => [country];
}

class LikeCountryState extends CovidState {
  final bool favorite;

  LikeCountryState(this.favorite);
  @override
  List<Object> get props => [favorite, UniqueKey()];

}
