import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CovidEvent extends Equatable {}

class InitialCovidEvent extends CovidEvent {
  @override
  List<Object> get props => [];
}

class GetCovidSummaryEvent extends CovidEvent {
  @override
  List<Object> get props => [];
}

class NavigationButtonEvent extends CovidEvent {
  final int currentTab;

  NavigationButtonEvent({@required this.currentTab});

  @override
  List<Object> get props => [currentTab];
}

class GetCovidSummaryByCountryEvent extends CovidEvent {
  final Country country;

  GetCovidSummaryByCountryEvent(this.country);

  @override
  List<Object> get props => [country];
}

class GetLearnMorePageEvent extends CovidEvent {
  @override
  List<Object> get props => [];
}

class ShowSettingsPageEvent extends CovidEvent {
  @override
  List<Object> get props => [];
}

class SetFavoriteCountryEvent extends CovidEvent {
  final Country country;

  SetFavoriteCountryEvent(this.country);

  @override
  List<Object> get props => [country];
}

class GetFavoriteCountryEvent extends CovidEvent {

  @override
  List<Object> get props => [];
}