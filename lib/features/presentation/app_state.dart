import 'package:covid19tracker/features/domain/entities/covid_summary.dart';

class AppState {
  List<Country> _getFavoriteCountries;

  List<Country> get getFavoriteCountries => _getFavoriteCountries;

  set setFavoriteCountries(List<Country> value) {
    _getFavoriteCountries = value;
  }
}
