import 'package:covid19tracker/util/shared.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LearnMorePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        ExpansionWidget(
          title: "Stay home except to get medical care",
          leading: FontAwesomeIcons.home,
          children: SharedData.stayHome,
        ),
        ExpansionWidget(
          leading: FontAwesomeIcons.peopleArrows,
          title: "Separate yourself from other people",
          children: SharedData.separateYourself,
        ),
        ExpansionWidget(
          leading: FontAwesomeIcons.temperatureHigh,
          title: "Monitor your symptoms",
          children: SharedData.monitorSymptoms,
        ),
        ExpansionWidget(
          leading: FontAwesomeIcons.phone,
          title: "Call ahead before visiting your doctor",
          children: SharedData.callAhead,
        ),
        ExpansionWidget(
          leading: FontAwesomeIcons.mask,
          title:
              " If you are sick wear a cloth covering over your nose and mouth",
          children: SharedData.sickWear,
        ),
        ExpansionWidget(
          leading: FontAwesomeIcons.boxTissue,
          title: "Cover your coughs and sneezes",
          children: SharedData.cover,
        ),
        ExpansionWidget(
          leading: FontAwesomeIcons.handsWash,
          title: "Clean your hands often",
          children: SharedData.cleanHands,
        ),
        ExpansionWidget(
          leading: FontAwesomeIcons.virusSlash,
          title: "Avoid sharing personal household items",
          children: SharedData.avoidSharing,
        ),
        ExpansionWidget(
          leading: FontAwesomeIcons.pumpSoap,
          title: "Clean all “high-touch” surfaces everyday",
          children: SharedData.clean,
        )
      ],
    );
  }
}

class ExpansionWidget extends StatelessWidget {
  final IconData leading;
  final String title;
  final List<String> children;

  const ExpansionWidget({
    Key key,
    @required this.leading,
    @required this.title,
    @required this.children,
  }) : super(key: key);

  List<Widget> buildExpansionWidget() {
    List<Widget> list = [];
    for (final item in children) {
      list.add(
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              BlackDotWidget(),
              Expanded(
                child: Text(item),
              ),
            ],
          ),
        ),
      );
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
        leading: FaIcon(leading),
        title: Text(title),
        children: [
          Column(
            children: buildExpansionWidget(),
          )
        ]);
  }
}

class BlackDotWidget extends StatelessWidget {
  const BlackDotWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      margin: EdgeInsets.all(16.0),
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black),
      height: 5,
      width: 5,
    );
  }
}
