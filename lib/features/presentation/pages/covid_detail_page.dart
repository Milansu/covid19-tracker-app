import 'dart:math' as math;
import 'dart:ui' as ui;
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:covid19tracker/util/shared.dart';
import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:covid19tracker/features/presentation/bloc/bloc.dart';
import 'package:covid19tracker/features/presentation/bloc/covid_bloc.dart';

class CovidDetailPage extends StatefulWidget {
  final Country country;

  CovidDetailPage(this.country);

  @override
  _CovidDetailPageState createState() => _CovidDetailPageState();
}

class _CovidDetailPageState extends State<CovidDetailPage> {
  Color _colorStart, _colorEnd;

  @override
  void initState() {
    _colorStart =
        Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0);

    _colorEnd =
        Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Column(children: [
          CustomPaint(
            painter: DrawCircle(
                colorStart: _colorStart,
                colorEnd: _colorEnd,
                offset: Offset(120, 70),
                radius: 160.0),
          ),
          Padding(
            padding: EdgeInsets.only(top: 50),
            child: CircleAvatar(
              radius: 81,
              backgroundColor: Colors.black,
              child: CircleAvatar(
                radius: 80,
                child: ClipOval(
                  child: Flag(
                    widget.country.countryCode,
                    width: 400,
                    height: 225,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    widget.country.country,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 28.0,
                    ),
                  ),
                ),
                LikeCountry(widget: widget.country)
              ],
            ),
          ),
        ]),
        GridView.count(
            shrinkWrap: true,
            crossAxisSpacing: 6.0,
            physics: NeverScrollableScrollPhysics(),
            crossAxisCount: 2,
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Card(
                  elevation: 16.0,
                  child: Stack(children: [
                    Container(
                      alignment: FractionalOffset.center,
                      child: FaIcon(
                        FontAwesomeIcons.virus,
                        size: 60.0,
                      ),
                      height: 200,
                      width: 190,
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        "Confirmed cases",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w300),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                          SharedData.formatter
                              .format(widget.country.totalConfirmed),
                          style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey)),
                    )
                  ]),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Card(
                  elevation: 16.0,
                  child: Stack(children: [
                    Container(
                      alignment: FractionalOffset.center,
                      child: FaIcon(
                        FontAwesomeIcons.running,
                        size: 60.0,
                      ),
                      height: 200,
                      width: 190,
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        "Total recovered",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w300),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                          SharedData.formatter
                              .format(widget.country.totalRecovered),
                          style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey)),
                    )
                  ]),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Card(
                  elevation: 16.0,
                  child: Stack(children: [
                    Container(
                      alignment: FractionalOffset.center,
                      child: FaIcon(
                        FontAwesomeIcons.headSideVirus,
                        size: 60.0,
                      ),
                      height: 200,
                      width: 190,
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        "New confirmed",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w300),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                          SharedData.formatter
                              .format(widget.country.newConfirmed),
                          style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey)),
                    )
                  ]),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Card(
                  elevation: 16.0,
                  child: Stack(children: [
                    Container(
                      alignment: FractionalOffset.center,
                      child: FaIcon(
                        FontAwesomeIcons.shieldVirus,
                        size: 60.0,
                      ),
                      height: 200,
                      width: 190,
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        "New recovered",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w300),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                          SharedData.formatter
                              .format(widget.country.newRecovered),
                          style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey)),
                    )
                  ]),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Card(
                  elevation: 16.0,
                  child: Stack(children: [
                    Container(
                      alignment: FractionalOffset.center,
                      child: FaIcon(
                        FontAwesomeIcons.bookDead,
                        size: 60.0,
                      ),
                      height: 200,
                      width: 190,
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        "Total deaths",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w300),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                          SharedData.formatter
                              .format(widget.country.totalDeaths),
                          style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey)),
                    )
                  ]),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Card(
                  elevation: 16.0,
                  child: Stack(children: [
                    Container(
                      alignment: FractionalOffset.center,
                      child: FaIcon(
                        FontAwesomeIcons.skull,
                        size: 60.0,
                      ),
                      height: 200,
                      width: 190,
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        "New deaths",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w300),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                          SharedData.formatter.format(widget.country.newDeaths),
                          style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey)),
                    )
                  ]),
                ),
              ),
            ]),
      ],
    );
  }
}

class LikeCountry extends StatelessWidget {
   LikeCountry({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final Country widget;

  @override
  Widget build(BuildContext context) {
    print("rebuild");
    return BlocBuilder<CovidBloc, CovidState>(
        condition: (previousState, currentState) => currentState is LikeCountry,
        builder: (BuildContext context, CovidState state) {
          print(state);
          if (state is LikeCountryState) {
            print('state ${state.favorite}');
            return InkWell(
              onTap: () {
                BlocProvider.of<CovidBloc>(context)
                    .add(SetFavoriteCountryEvent(widget));
              },
              child: Align(
                  alignment: Alignment.centerRight,
                  child: state.favorite
                      ? Icon(Icons.favorite)
                      : Icon(Icons.favorite_border)),
            );
          }
          return InkWell(
            onTap: () {
              BlocProvider.of<CovidBloc>(context)
                  .add(SetFavoriteCountryEvent(widget));
            },
            child: Align(
                alignment: Alignment.centerRight,
                child: Icon(Icons.favorite_border)),
          );
        });
  }
}

class DrawCircle extends CustomPainter {
  final Color colorStart, colorEnd;
  final Offset offset;
  final double radius;

  DrawCircle({this.colorStart, this.colorEnd, this.offset, this.radius});

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..shader = ui.Gradient.linear(
          Offset(-0, 250), Offset(-400, 100), [colorStart, colorEnd]);
    canvas.drawCircle(offset, radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}
