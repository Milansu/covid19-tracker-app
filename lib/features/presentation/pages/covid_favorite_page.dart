import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:covid19tracker/features/presentation/bloc/bloc.dart';
import 'package:covid19tracker/features/presentation/bloc/covid_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CovidFavoritePage extends StatelessWidget {
  final List<Country> country;

  const CovidFavoritePage({Key key, this.country}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: country.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          onTap: () => BlocProvider.of<CovidBloc>(context)
              .add(GetCovidSummaryByCountryEvent(country[index])),
          leading: Flag(
            country[index].countryCode,
            height: 60,
            width: 60,
            fit: BoxFit.contain,
          ),
          title: Text(country[index].country),
        );
      },
    );
  }
}
