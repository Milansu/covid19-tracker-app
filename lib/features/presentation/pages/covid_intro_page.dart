import 'dart:math' as math;

import 'package:covid19tracker/features/presentation/bloc/bloc.dart';
import 'package:covid19tracker/features/presentation/bloc/covid_bloc.dart';
import 'package:covid19tracker/util/shared.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CovidIntroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(16.0),
      shrinkWrap: true,
      children: [
        InkWell(
          onTap: () {
            BlocProvider.of<CovidBloc>(context).add(GetLearnMorePageEvent());
          },
          child: Stack(
            children: [
              Container(
                height: 200.0,
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Color(0xFF003D64),
                    borderRadius: BorderRadius.circular(20.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black45,
                          offset: Offset(0.0, 10.0),
                          blurRadius: 10.0)
                    ]),
              ),
              Container(
                alignment: FractionalOffset.centerRight,
                child: SvgPicture.asset(
                  SharedData.maskMan,
                ),
                height: 200,
                width: 190,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, top: 30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Steps to help prevent the spread',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24.0,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    Container(
                      height: 40.0,
                      width: 150.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Color(0xFF00578D),
                      ),
                      child: Center(
                        child: RichText(
                          text: TextSpan(
                            text: 'Learn More',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                            children: [
                              WidgetSpan(
                                child: Icon(
                                  Icons.arrow_forward,
                                  color: Colors.white,
                                ),
                                alignment: PlaceholderAlignment.middle,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(height: 16.0),
        GridView.count(
          shrinkWrap: true,
          crossAxisSpacing: 6.0,
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          children: <Widget>[
            InkWell(
              onTap: () => BlocProvider.of<CovidBloc>(context)
                  .add(GetCovidSummaryEvent()),
              child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color:
                        Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
                            .withOpacity(1.0),
                    borderRadius: BorderRadius.circular(20.0)),
                child: Stack(
                  children: [
                    Container(
                      alignment: FractionalOffset.centerRight,
                      child: SvgPicture.asset(
                        SharedData.remoteWorkWoman,
                      ),
                      height: 200,
                      width: 190,
                    ),
                    Container(
                      alignment: FractionalOffset.bottomCenter,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.black38,
                            borderRadius: BorderRadius.circular(20.0)),
                        child: RichText(
                          text: TextSpan(
                            text: 'Statistics',
                            style: TextStyle(
                                decorationStyle: TextDecorationStyle.dotted,
                                color: Colors.white,
                                decorationThickness: 2,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
                      .withOpacity(1.0),
                  borderRadius: BorderRadius.circular(20.0)),
              child: Stack(
                children: [
                  Container(
                    alignment: FractionalOffset.centerRight,
                    child: SvgPicture.asset(
                      SharedData.doctorMan,
                    ),
                    height: 200,
                    width: 190,
                  ),
                  Container(
                    alignment: FractionalOffset.bottomCenter,
                    child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.black38,
                          borderRadius: BorderRadius.circular(20.0)),
                      child: RichText(
                        text: TextSpan(
                          text: 'Symptoms',
                          style: TextStyle(
                              decorationStyle: TextDecorationStyle.dotted,
                              color: Colors.white,
                              decorationThickness: 2,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
