import 'package:flutter/material.dart';
import 'package:theme_provider/theme_provider.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            child: Text("Change theme"),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (_) => ThemeConsumer(child: ThemeDialog()));
            },
          ),
        ],
      ),
    );
  }
}
