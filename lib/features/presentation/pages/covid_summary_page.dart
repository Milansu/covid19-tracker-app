import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:covid19tracker/features/presentation/bloc/covid_bloc.dart';
import 'package:covid19tracker/features/presentation/bloc/covid_event.dart';
import 'package:covid19tracker/util/shared.dart';
import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CovidSummaryPage extends StatefulWidget {
  final CovidSummary covidSummary;

  const CovidSummaryPage({Key key, @required this.covidSummary})
      : super(key: key);

  @override
  _CovidSummaryPageState createState() => _CovidSummaryPageState();
}

class _CovidSummaryPageState extends State<CovidSummaryPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SvgPicture.asset("assets/images/virus.svg", height: 200,),
        Text(
          "Lasted updated on: ${widget.covidSummary.date.toLocal()}",
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: AutoCompleteTextField<Country>(
            key: GlobalKey<AutoCompleteTextFieldState<Country>>(),
            itemFilter: (item, query) {
              return item.country.toLowerCase().startsWith(query.toLowerCase());
            },
            itemSorter: (a, b) {
              return a.country.compareTo(b.country);
            },
            itemSubmitted: (country) {
              BlocProvider.of<CovidBloc>(context)
                  .add(GetCovidSummaryByCountryEvent(country));
            },
            suggestions: widget.covidSummary.countries,
            decoration: InputDecoration(
                labelText: "Search by country",
                hintText: "Search by country",
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25.0)))),
            itemBuilder: (BuildContext context, Country country) {
              return Row(
                children: [
                  Flag(
                    country.countryCode,
                    fit: BoxFit.contain,
                    height: 30,
                    width: 30,
                  ),
                  Text(country.country),
                ],
              );
            },
          ),
        ),
        Text("Confirmed cases",
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w300)),
        Text(
            SharedData.formatter
                .format(widget.covidSummary.global.totalConfirmed),
            style: TextStyle(
                fontSize: 28, fontWeight: FontWeight.bold, color: Colors.grey)),
        Text("Total deaths",
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w300)),
        Text(
            SharedData.formatter.format(widget.covidSummary.global.totalDeaths),
            style: TextStyle(
                fontSize: 28, fontWeight: FontWeight.bold, color: Colors.grey)),
        Text("Total recovered",
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w300)),
        Text(
            SharedData.formatter
                .format(widget.covidSummary.global.totalRecovered),
            style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
                color: Colors.green)),
        Text("New confirmed",
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w300)),
        Text(
            SharedData.formatter
                .format(widget.covidSummary.global.newConfirmed),
            style: TextStyle(
                fontSize: 28, fontWeight: FontWeight.bold, color: Colors.grey)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Text("New recovered",
                    style:
                    TextStyle(fontSize: 24, fontWeight: FontWeight.w300)),
                Text(
                    SharedData.formatter
                        .format(widget.covidSummary.global.newRecovered),
                    style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
              ],
            ),
            Column(
              children: [
                Text("New deaths",
                    style:
                    TextStyle(fontSize: 24, fontWeight: FontWeight.w300)),
                Row(
                  children: [
                    Text(
                        SharedData.formatter
                            .format(widget.covidSummary.global.newDeaths),
                        style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey)),
                  ],
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
