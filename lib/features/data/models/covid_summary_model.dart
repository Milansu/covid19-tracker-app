import 'dart:convert';

import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:meta/meta.dart';

class CovidSummaryModel extends CovidSummary {
  CovidSummaryModel({
    @required GlobalModel global,
    @required List<CountryModel> countries,
    @required DateTime date,
  }) : super(global: global, countries: countries, date: date);

  factory CovidSummaryModel.fromRawJson(String str) =>
      CovidSummaryModel.fromJson(json.decode(str));

  factory CovidSummaryModel.fromJson(Map<String, dynamic> json) =>
      CovidSummaryModel(
        global: json["Global"] == null
            ? null
            : GlobalModel.fromJson(json["Global"]),
        countries: json["Countries"] == null
            ? null
            : List<CountryModel>.from(
            json["Countries"].map((x) => CountryModel.fromJson(x))),
        date: json["Date"] == null ? null : DateTime.parse(json["Date"]),
      );
}

class CountryModel extends Country {
  CountryModel({
    @required String country,
    @required String countryCode,
    @required String slug,
    @required int newConfirmed,
    @required int totalConfirmed,
    @required int newDeaths,
    @required int totalDeaths,
    @required int newRecovered,
    @required int totalRecovered,
    @required DateTime date,
  }) : super(
      country: country,
      countryCode: countryCode,
      slug: slug,
      newConfirmed: newConfirmed,
      totalConfirmed: totalConfirmed,
      newDeaths: newDeaths,
      totalDeaths: totalDeaths,
      newRecovered: newRecovered,
      totalRecovered: totalRecovered,
      date: date);

  factory CountryModel.fromRawJson(String str) =>
      CountryModel.fromJson(json.decode(str));

  factory CountryModel.fromJson(Map<String, dynamic> json) => CountryModel(
    country: json["Country"] == null ? null : json["Country"],
    countryCode: json["CountryCode"] == null ? null : json["CountryCode"],
    slug: json["Slug"] == null ? null : json["Slug"],
    newConfirmed:
    json["NewConfirmed"] == null ? null : json["NewConfirmed"],
    totalConfirmed:
    json["TotalConfirmed"] == null ? null : json["TotalConfirmed"],
    newDeaths: json["NewDeaths"] == null ? null : json["NewDeaths"],
    totalDeaths: json["TotalDeaths"] == null ? null : json["TotalDeaths"],
    newRecovered:
    json["NewRecovered"] == null ? null : json["NewRecovered"],
    totalRecovered:
    json["TotalRecovered"] == null ? null : json["TotalRecovered"],
    date: json["Date"] == null ? null : DateTime.parse(json["Date"]),
  );

  Map<String, dynamic> toJson() =>
      {
        "Country": country,
        "CountryCode": countryCode,
        "Slug": slug,
        "NewConfirmed": newConfirmed,
        "TotalConfirmed": totalConfirmed,
        "NewDeaths": newDeaths,
        "TotalDeaths": totalDeaths,
        "NewRecovered": newRecovered,
        "TotalRecovered": totalRecovered,
        "Date": date.toIso8601String(),
      };
}

class GlobalModel extends Global {
  GlobalModel({
    @required int newConfirmed,
    @required int totalConfirmed,
    @required int newDeaths,
    @required int totalDeaths,
    @required int newRecovered,
    @required int totalRecovered,
  }) : super(
    newConfirmed: newConfirmed,
    totalConfirmed: totalConfirmed,
    newDeaths: newDeaths,
    totalDeaths: totalDeaths,
    newRecovered: newRecovered,
    totalRecovered: totalRecovered,
  );

  factory GlobalModel.fromRawJson(String str) =>
      GlobalModel.fromJson(json.decode(str));

  factory GlobalModel.fromJson(Map<String, dynamic> json) => GlobalModel(
    newConfirmed:
    json["NewConfirmed"] == null ? null : json["NewConfirmed"],
    totalConfirmed:
    json["TotalConfirmed"] == null ? null : json["TotalConfirmed"],
    newDeaths: json["NewDeaths"] == null ? null : json["NewDeaths"],
    totalDeaths: json["TotalDeaths"] == null ? null : json["TotalDeaths"],
    newRecovered:
    json["NewRecovered"] == null ? null : json["NewRecovered"],
    totalRecovered:
    json["TotalRecovered"] == null ? null : json["TotalRecovered"],
  );
}
