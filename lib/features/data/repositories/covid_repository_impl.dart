import 'package:covid19tracker/core/error/exceptions.dart';
import 'package:covid19tracker/core/error/failures.dart';
import 'package:covid19tracker/core/network/network_info.dart';
import 'package:covid19tracker/features/data/datasources/covid_local_data_source.dart';
import 'package:covid19tracker/features/data/datasources/covid_remote_data_source.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:covid19tracker/features/domain/repositories/covid_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

class CovidRepositoryImpl extends CovidRepository {
  final CovidRemoteDataSource _covidRemoteDataSource;
  final CovidLocalDataSource _covidLocalDataSource;
  final NetworkInfo _networkInfo;

  CovidRepositoryImpl(this._covidRemoteDataSource, this._covidLocalDataSource,
      this._networkInfo);

  @override
  Future<Either<Failure, CovidSummary>> getCovidSummary() async {
    if (await _networkInfo.isConnected) {
      try {
        final _remoteCovidSummary =
            await _covidRemoteDataSource.getCovidSummary();
        return Right(_remoteCovidSummary);
      } on ServerException {
        return Left(ServerFailure());
      }
    }
    return Left(NoNetworkConnectionFailure());
  }

  @override
  Future<Either<Failure, List<Country>>> getFavoriteCountry() async {
    try {
      final _favoriteCountry = await _covidLocalDataSource.getFavoriteCountry();
      return Right(_favoriteCountry);
    } catch (error) {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> setFavoriteCountry({@required Country country, bool removeItem}) async {
    try {
      final _favoriteCountry =
          await _covidLocalDataSource.setFavoriteCountry(country: country, removeItem: removeItem);
      if (_favoriteCountry) {
        return Right(_favoriteCountry);
      }
      return Left(WriteToStorageFailure());
    } catch (_) {
      return Left(WriteToStorageFailure());
    }
  }
}
