import 'dart:convert';

import 'package:covid19tracker/core/error/exceptions.dart';
import 'package:covid19tracker/features/data/models/covid_summary_model.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class CovidLocalDataSource {
  /// Calls the https://api.covid19api.com/summary endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<List<Country>> getFavoriteCountry();

  Future<bool> setFavoriteCountry({@required CountryModel country, bool removeItem});
}

class CovidLocalDataSourceImpl implements CovidLocalDataSource {
  final SharedPreferences sharedPreferences;

  CovidLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<List<Country>> getFavoriteCountry() {
    final List<String> jsonString =
        sharedPreferences.getStringList('get_country');
    List<Country> list = [];
    if (json != null) {
      for (int i = 0; i < jsonString.length; i++) {
        list.add(CountryModel.fromJson(json.decode(jsonString[i])));
      }
      return Future.value(list);
    } else {
      throw CacheException();
    }
  }

  @override
  Future<bool> setFavoriteCountry({@required CountryModel country, bool removeItem = false}) async {
    final jsonString = sharedPreferences.getStringList('get_country');
    if (removeItem) {
      jsonString.remove(json.encode(
        country.toJson(),
      ));
      return await sharedPreferences.setStringList('get_country', jsonString);
    }
    else {
      //todo check if it already contains  the same model
      if (jsonString != null && jsonString.isNotEmpty) {
        jsonString.add(json.encode(
          country.toJson(),
        ));
        return await sharedPreferences.setStringList('get_country', jsonString);
      }
      else {
        return await sharedPreferences.setStringList(
            'get_country', [json.encode(
          country.toJson(),
        )
        ]);
      }
    }
  }
}
