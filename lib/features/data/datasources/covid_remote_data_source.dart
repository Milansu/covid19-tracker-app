import 'dart:convert';

import 'package:covid19tracker/core/error/exceptions.dart';
import 'package:covid19tracker/features/data/models/covid_summary_model.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

abstract class CovidRemoteDataSource {
  /// Calls the https://api.covid19api.com/summary endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<CovidSummary> getCovidSummary();
}

class CovidRemoteDataSourceImpl implements CovidRemoteDataSource {
  final http.Client client;

  CovidRemoteDataSourceImpl({@required this.client});

  @override
  Future<CovidSummary> getCovidSummary() async {
    final response = await client.get(
      "https://api.covid19api.com/summary",
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return CovidSummaryModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
