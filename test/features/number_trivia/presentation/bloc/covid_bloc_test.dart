import 'package:bloc_test/bloc_test.dart';
import 'package:covid19tracker/features/domain/usecases/get_covid_summary.dart';
import 'package:covid19tracker/features/presentation/bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../util/mocks.dart';

class MockGetCovidSummary extends Mock implements GetCovidSummary {}

void main() {
  CovidBloc bloc;
  MockGetCovidSummary mockGetCovidSummary;

  setUp(() {
    mockGetCovidSummary = MockGetCovidSummary();

    bloc = CovidBloc(mockGetCovidSummary);
  });
  //TODO  Check why naming test's like """test""", is not available in this version of bloc_test
  group("GetCovidSummaryEvent", () {
    /**
     * when:
     *      GetCovidSummaryEvent() is added to the bloc
     * bloc should emit:
     *      LoadedCovidState()
     * verify:
     *      mockGetCovidSummary() is called once
     */
    blocTest('GetCovidSummaryEvent',
        build: () async {
          when(mockGetCovidSummary())
              .thenAnswer((realInvocation) async => (Right(covidSummaryModel)));
          return bloc;
        },
        act: (bloc) => bloc.add(GetCovidSummaryEvent()),
        expect: [LoadedCovidState(covidSummaryModel)],
        verify: (CovidBloc bloc) async {
          verify(mockGetCovidSummary()).called(1);
          verifyNoMoreInteractions(mockGetCovidSummary);
        });
  });

  group("GetLearnMorePageEvent", () {
    /**
     * when:
     *      GetLearnMorePageEvent() is added to the bloc
     * bloc should emit:
     *      LoadedLearnMoreState()
     */
    blocTest('GetLearnMorePageEvent',
        build: () async {
          return bloc;
        },
        act: (bloc) => bloc.add(GetLearnMorePageEvent()),
        expect: [LoadedLearnMoreState()]);
  });

  group("GetCovidSummaryByCountryEvent", () {
    /**
     * when:
     *      GetCovidSummaryByCountryEvent() is added to the bloc
     * bloc should emit:
     *      LoadedCovidByCountryState()
     */
    blocTest('GetLearnMorePageEvent',
        build: () async {
          return bloc;
        },
        act: (bloc) =>
            bloc.add(GetCovidSummaryByCountryEvent(
                covidSummaryModel.countries.first)),
        expect: [LoadedCovidByCountryState(covidSummaryModel.countries.first)]);
  });
}
