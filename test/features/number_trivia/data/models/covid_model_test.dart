import 'dart:convert';

import 'package:covid19tracker/features/data/models/covid_summary_model.dart';
import 'package:covid19tracker/features/domain/entities/covid_summary.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';
import '../../../../util/mocks.dart';

void main() {

  test(
    'should be a subclass of CovidSummary entity',
    () async {
      // assert
      expect(covidSummaryModel, isA<CovidSummary>());
    },
  );

  group('fromJson', () {
    test(
      'should return a valid model when the JSON number is an integer',
      () async {
        // arrange
        final Map<String, dynamic> jsonMap =
            json.decode(fixture('covid.json'));
        // act
        final result = CovidSummaryModel.fromJson(jsonMap);
        // assert
        expect(result, covidSummaryModel);
      },
    );
  });
}
