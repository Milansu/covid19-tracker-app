
import 'package:covid19tracker/features/domain/repositories/covid_repository.dart';
import 'package:covid19tracker/features/domain/usecases/get_covid_summary.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../util/mocks.dart';

class MockCovidRepository extends Mock
    implements CovidRepository {}

void main() {
  GetCovidSummary usecase;
  MockCovidRepository mockCovidRepository;

  setUp(() {
    mockCovidRepository = MockCovidRepository();
    usecase = GetCovidSummary(mockCovidRepository);
  });

  final _covidSummary = covidSummaryModel;

  test(
    'should get CovidSummary for the number from the repository',
    () async {
      // arrange
      when(mockCovidRepository.getCovidSummary())
          .thenAnswer((_) async => Right(_covidSummary));
      // act
      final result = await usecase();
      // assert
      expect(result, Right(_covidSummary));
      verify(mockCovidRepository.getCovidSummary());
      verifyNoMoreInteractions(mockCovidRepository);
    },
  );
}
