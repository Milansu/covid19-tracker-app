import 'package:covid19tracker/features/data/models/covid_summary_model.dart';

final covidSummaryModel = CovidSummaryModel(
    countries: [
      CountryModel(
          countryCode: 'srb',
          country: 'Serbia',
          date: DateTime.parse("2020-06-19T17:58:38Z"),
          newConfirmed: 1,
          newDeaths: 1,
          newRecovered: 1,
          totalConfirmed: 1,
          totalDeaths: 1,
          totalRecovered: 1,
          slug: 'rs')
    ],
    date: DateTime.parse("2020-06-19T17:58:38Z"),
    global: GlobalModel(
        totalDeaths: 1,
        totalConfirmed: 1,
        newRecovered: 1,
        newDeaths: 1,
        newConfirmed: 1,
        totalRecovered: 1));